import React, {Component} from 'react';
import {Header, Container, Grid, Item, Image,  Segment, Radio,Form, Button} from 'semantic-ui-react';
import {Link} from "react-router-dom";

export default class Purchase extends Component {
    state = {
        value: '신용카드',
    }
    handleChange = (e, { value }) => this.setState({ value })
  
    render() {
        return(
            <Container>
                <div className="title_area">
                    <Header>
                        주문서 작성
                    </Header>
                </div>
                <div className="product_list">
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={2} textAlign="center">
                                <Image as="div" src="images/temp/product-01.png" size="tiny"></Image>
                            </Grid.Column>
                            <Grid.Column width={14}>
                                <Item>
                                    <Item.Header>
                                    [BOSS] 보스 사운드링크 리볼브+ 블루투스 스피커SoundLink Revolve+ Bluetooth speaker
                                    </Item.Header>
                                    <Item.Description>
                                        <span>수량 <b>1</b>개</span>
                                        <span className="price"><b>150,000</b>원</span>
                                    </Item.Description>
                                </Item>
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row>
                            <Grid.Column width={2} textAlign="center">
                                <Image as="div" src="images/temp/product-05.png" size="tiny"></Image>
                            </Grid.Column>
                            <Grid.Column width={14}>
                                <Item>
                                    <Item.Header>
                                        [셀렙샵] 셀렙샵 에디션 20FW 마이 발렌타인 드레스
                                    </Item.Header>
                                    <Item.Description>
                                        <span>수량 <b>1</b>개</span>
                                        <span className="price"><b>150,000</b>원</span>
                                    </Item.Description>
                                </Item>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column width={2} textAlign="center">
                                <Image as="div" src="images/temp/product-04.png" size="tiny"></Image>
                            </Grid.Column>
                            <Grid.Column width={14}>
                                <Item>
                                    <Item.Header>
                                    [키엘] 키엘 칼렌듈라 허벌 엑스트렉트 토너 500ml
                                    </Item.Header>
                                    <Item.Description>
                                        <span>수량 <b>1</b>개</span>
                                        <span className="price"><b>150,000</b>원</span>
                                    </Item.Description>
                                </Item>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>

                <div className="purchase_info">
                    <Segment>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={12}>
                                    <Grid.Row>
                                        <Header>배송지 정보</Header>
                                        <Segment className="delivery_info">
                                            <p>홍길동(기본배송지)</p>
                                            <p>010-1234-5678</p>
                                            <p>(40010)서울시 금천구 벚꽃로 298 대륭포스트6차 2층</p>
                                        </Segment>
                                    </Grid.Row>
                                    <Grid.Row  className="pay">
                                        <Header>결제수단</Header>
                                    <Form>
                                        <Form.Field>
                                        <Radio
                                            label='신용카드'
                                            name='radioGroup'
                                            value='신용카드'
                                            checked={this.state.value === '신용카드'}
                                            onChange={this.handleChange}
                                        />
                                        </Form.Field>
                                        <Form.Field>
                                        <Radio
                                            label='계좌 간편결제'
                                            name='radioGroup'
                                            value='계좌 간편결제'
                                            checked={this.state.value === '계좌 간편결제'}
                                            onChange={this.handleChange}
                                        />
                                        </Form.Field>
                                        <Form.Field>
                                        <Radio
                                            label='카드 간편결제'
                                            name='radioGroup'
                                            value='카드 간편결제'
                                            checked={this.state.value === '카드 간편결제'}
                                            onChange={this.handleChange}
                                        />
                                        </Form.Field>
                                        <Form.Field>
                                        <Radio
                                            label='충전포인트 결제'
                                            name='radioGroup'
                                            value='충전포인트 결제'
                                            checked={this.state.value === '충전포인트 결제'}
                                            onChange={this.handleChange}
                                        />
                                        </Form.Field>
                                    </Form>
                                    </Grid.Row>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <div className="price_wrap">
                                        <p className="bold">결제금액</p>
                                        <p className="total_price">250,000 <span>원</span></p>
                                    </div>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Segment> 
                </div>
                <div className="purchase_button">    
                <Link to="/finish_purchase"> 
                    <Button primary size="large">결제하기</Button>
                </Link>  
                </div>
            </Container> 
        )
    }
} 