import React, {Component} from 'react';
import { Container, Header, Grid, Button,Image, Item , Checkbox} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
class Basket extends Component {
    render() {

        return (
            <>
                <Container>
                    <Header className="bk_title">
                        장바구니
                    </Header>
                    <Grid divided='vertically'>
                        <Grid.Row columns={1} textAlign="left">
                            <Grid.Column width={3}>
                                <Checkbox label='전체선택' />
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row columns={3} >
                            <Grid.Column width={2}>
                                <Checkbox className="mg15"/>
                                <Image src='/images/temp/product-01.png' size="tiny" verticalAlign='middle' className="inline-block" bordered />
                            </Grid.Column>
                            <Grid.Column width={11}>
                                <Item>
                                    <Item.Content>
                                        <Item.Header>
                                            <p className="bold">[BOSS]보스 사운드 링크 리볼브+ 블루투스 스피커 SoundLink Revolve+ Bluetooth speaker</p>
                                        </Item.Header>
                                        <Item.Description>
                                            <p className="inline-block mg10">수량 <span className="bold">1</span>개</p>
                                            <p className="inline-block"><span className="bold">150,000</span>원</p>   
                                        </Item.Description>
                                    </Item.Content>
                                </Item>
                            </Grid.Column>
                            <Grid.Column width={3} textAlign="right">
                                <Button basic>구매하기</Button>
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row columns={3} >
                            <Grid.Column width={2}>
                                <Checkbox className="mg15"/>
                                <Image src='/images/temp/product-01.png' size="tiny" verticalAlign='middle' className="inline-block" bordered />
                            </Grid.Column>
                            <Grid.Column width={11}>
                                <Item>
                                    <Item.Content>
                                        <Item.Header>
                                            <p className="bold">[BOSS]보스 사운드 링크 리볼브+ 블루투스 스피커 SoundLink Revolve+ Bluetooth speaker</p>
                                        </Item.Header>
                                        <Item.Description>
                                            <p className="inline-block mg10">수량 <span className="bold">1</span>개</p>
                                            <p className="inline-block"><span className="bold">150,000</span>원</p>                                      
                                        </Item.Description>
                                    </Item.Content>
                                </Item>
                            </Grid.Column>
                            <Grid.Column width={3} textAlign="right">
                                <Button basic>구매하기</Button>
                            </Grid.Column>
                        </Grid.Row>
 
                        <Grid.Row>
                            <Grid.Column textAlign="right">
                                <p className="inline-block mg15">주문 총 금액</p>
                                <p className="inline-block total_price">250,000 <span>원</span></p> 
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <div className="align_center bk_button_group">
                        <Link to="/purchase">
                            <Button size="large" basic className="select_btn" >
                                선택상품 구매
                            </Button>
                            <Button size="large" primary>
                                전체상품 구매
                            </Button>
                        </Link>
                    </div>
                </Container>
            </>
        );
    }
}

export default Basket;