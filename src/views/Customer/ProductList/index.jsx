import React, { Component } from 'react';
import {Grid, Container, Item, Button, Modal, Image} from 'semantic-ui-react';
import {Link} from 'react-router-dom';


function exampleReducer(state, action) {
    switch (action.type) {
      case 'close':
        return { open: false }
      case 'open':
        return { open: true}
      default:
        throw new Error('Unsupported action...')
    }
  }

const ProductList = () => {

    const [state, dispatch] = React.useReducer(exampleReducer, {
        open: false,
      })
      const { open } = state

    return(
        <>
        <Container>
            <div className="product-list">
            <Grid>
                <Grid.Row  columns={5}>
                    <Grid.Column>
                            <Item>
                                <Link to="/detail">
                                    <Item.Image size="tiny" src='/images/temp/product-01.png'/>
                                </Link>
                                <Item.Content>
                                    <Item.Header as='a'>보스 사운드링크 리볼브+ 블루투스 스피커SoundLink Revolve Speaker</Item.Header>
                                    <Item.Description onClick={() => dispatch({ type: 'open'})}>
                                            BOSE (보스) / 미국
                                    </Item.Description>
                                    <Item.Meta>250,000원</Item.Meta>
                                </Item.Content>
                            </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-02.png'/>
                            <Item.Content>
                                <Item.Header as='a'>레무스 버킷백 블랙</Item.Header>
                                <Item.Description>모르간 / 이태리</Item.Description>
                                <Item.Meta>185,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-03.png'/>
                            <Item.Content>
                                <Item.Header as='a'>애플 아이폰Xs MAX iPhone Xs MAX 64G</Item.Header>
                                <Item.Description>APPLE / 미국</Item.Description>
                                <Item.Meta>740,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-04.png'/>
                            <Item.Content>
                                <Item.Header as='a'>칼렌듈라 허벌 엑스트렉트 토너 500ml</Item.Header>
                                <Item.Description>키엘 / 미국</Item.Description>
                                <Item.Meta>76,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-05.png'/>
                            <Item.Content>
                                <Item.Header as='a'>에디션 20FW 마이 발렌타인 드레스</Item.Header>
                                <Item.Description>셀렙샵 / 한국</Item.Description>
                                <Item.Meta>230,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row  columns={5}>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-06.png'/>
                            <Item.Content>
                                <Item.Header as='a'>타투 글래스 틴트</Item.Header>
                                <Item.Description>포렌코즈</Item.Description>
                                <Item.Meta>12,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-07.png'/>
                            <Item.Content>
                                <Item.Header as='a'>이오시카 가정용 IPL 레이저 제모의료기기 1000K SIPL-1000</Item.Header>
                                <Item.Description>이오시카</Item.Description>
                                <Item.Meta>540,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-08.png'/>
                            <Item.Content>
                                <Item.Header as='a'>스킨라이트 테라피 II 외 CJ 단독 세트</Item.Header>
                                <Item.Description>메이크온</Item.Description>
                                <Item.Meta>870,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-09.png'/>
                            <Item.Content>
                                <Item.Header as='a'>STRING BUCKET BAG 스트링 버킷백 SB100073 여성가방</Item.Header>
                                <Item.Description>케즈</Item.Description>
                                <Item.Meta>100,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-10.png'/>
                            <Item.Content>
                                <Item.Header as='a'>[송혜교슈즈] 보라보라 샌들 Pink_DG2AM20036PIK</Item.Header>
                                <Item.Description>슈콤마보니</Item.Description>
                                <Item.Meta>248,540원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                </Grid.Row>


                <Grid.Row  columns={5}>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-01.png'/>
                            <Item.Content>
                                <Item.Header as='a'>보스 사운드링크 리볼브+ 블루투스 스피커SoundLink Revolve Speaker</Item.Header>
                                <Item.Description>브랜드명</Item.Description>
                                <Item.Meta>000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-11.png'/>
                            <Item.Content>
                                <Item.Header as='a'>1898 블루펄 다이얼 실버 시계</Item.Header>
                                <Item.Description>페라가모</Item.Description>
                                <Item.Meta>889,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-12.png'/>
                            <Item.Content>
                                <Item.Header as='a'>레꼴뜨 와플기계 스마일 베이커</Item.Header>
                                <Item.Description>레꼴뜨</Item.Description>
                                <Item.Meta>59,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-13.png'/>
                            <Item.Content>
                                <Item.Header as='a'>마녀공장 퓨어 클렌징 오일 200mlx2 [+25ml]</Item.Header>
                                <Item.Description>마녀공장</Item.Description>
                                <Item.Meta>15,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-14.png'/>
                            <Item.Content>
                                <Item.Header as='a'>[나인] ♡썸머 시즌오프♡ 최대쿠폰혜택까지 원피스/티/스커트</Item.Header>
                                <Item.Description>브랜드명</Item.Description>
                                <Item.Meta>190,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                </Grid.Row>


                <Grid.Row  columns={5}>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-15.png'/>
                            <Item.Content>
                                <Item.Header as='a'>마녀공장 허브 클렌징 오일</Item.Header>
                                <Item.Description>마녀공장</Item.Description>
                                <Item.Meta>14,500원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-16.png'/>
                            <Item.Content>
                                <Item.Header as='a'>[공식 직수입]스티브매든 ELAINE 니트 플랫 슈즈</Item.Header>
                                <Item.Description>ELAINE</Item.Description>
                                <Item.Meta>69,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-17.png'/>
                            <Item.Content>
                                <Item.Header as='a'>소니 정품 PXW-Z190 4K 전문가용 캠코더</Item.Header>
                                <Item.Description>SONY</Item.Description>
                                <Item.Meta>5,280,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-18.png'/>
                            <Item.Content>
                                <Item.Header as='a'>벤하임Benheim 32,000RPM 2리터 대용량 초고속블렌더 BHB-3200B</Item.Header>
                                <Item.Description>벤하임</Item.Description>
                                <Item.Meta>230,000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                            <Item.Image size="tiny" src='/images/temp/product-19.png'/>
                            <Item.Content>
                                <Item.Header as='a'>[10인용] 쿠쿠 IH메탈릭커브드 2기압 압력밥솥 CRP-FHR107FG/FD</Item.Header>
                                <Item.Description>쿠쿠전자</Item.Description>
                                <Item.Meta>000원</Item.Meta>
                            </Item.Content>
                        </Item>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            </div>
        </Container>



        <Modal
        open={open}
        onClose={() => dispatch({ type: 'close' })}
        size="tiny"
        centered={false}
      >
        <Modal.Header>브랜드 상세</Modal.Header>
        <Modal.Content>
            <Grid>
                <Grid.Column width={4}>
                    <Image src="/images/temp/lg_logo.png" bordered/>
                </Grid.Column>
                <Grid.Column width={12}>
                    <Item>
                        <Item.Header>
                            LG전자
                        </Item.Header>
                        <Item.Meta>
                            대한민국<span>|</span>설립년도 1974
                        </Item.Meta>
                        <Item.Description>
                            <p>
                            LG그룹 계열의 종합 전자제품 기업. LG그룹의 창업주 구인회 회장이 설립한 금성사를 모체로 발전해왔다. 
                            국산 최초 라디오 개발을 시작으로, TV, 노트북, 휴대폰 및 가전제품 전 분야에 걸쳐 기술력을 쌓아 
                            세계 시장에서 인정받아 온 한국 대표 기업이다.특히 LG전자(주)의 TV 및 모니터 패널은 세계 표준으로 알려져 있다
                            </p>
                        </Item.Description>
                    </Item>
                </Grid.Column>
            </Grid>
        </Modal.Content>
        <Modal.Actions>
          <Button size="large" centered primary onClick={() => dispatch({ type: 'close' }) }>
            확인
          </Button>
        </Modal.Actions>
      </Modal>
        </>
    ) ;
  }

export default ProductList;
