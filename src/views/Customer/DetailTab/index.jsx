import React, {Component} from 'react';
import { Tab, Image} from 'semantic-ui-react';

const panes = [
    { menuItem: '상세설명', render: () => <Tab.Pane attached={false}>
        <div>
            <Image size="huge" src="images/temp/pd-detail01.png"/>
        </div>
    </Tab.Pane> },
    { menuItem: '혜택/배송', render: () => <Tab.Pane attached={false}>혜택/배송</Tab.Pane> },
    { menuItem: '상품평', render: () => <Tab.Pane attached={false}>상품평 Content</Tab.Pane> },
    { menuItem: 'Q&A', render: () => <Tab.Pane attached={false}>Q&A</Tab.Pane> },
  ]
  

const DetailTab = () => (
    <Tab className="tab_wrapper" menu={{ secondary: true, pointing: true }} panes={panes} />
  );
export default DetailTab;