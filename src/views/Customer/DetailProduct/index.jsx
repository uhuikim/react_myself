import React, {Component} from 'react';
import DetailTab from '../DetailTab'
import { Container, Grid , Image, Segment , Item, Input, Button, Icon, Divider} from 'semantic-ui-react';


class DetailProduct extends Component {

    render() {
        return(
            <>
            <Container>
                <Grid columns={2}>
                    <Grid.Column>
                        <Segment>
                            <Image size = "large"  src="images/temp/product-01.png"/>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Item>
                        <Item.Content>
                            <Item.Header>[BOSS] 보스 사운드링크 리볼브+ 블루투스 스피커SoundLink Revolve+ Bluetooth speaker</Item.Header>
                            <Item.Meta>
                                <div className="price">
                                    150,000<span>원</span>
                                </div>
                            </Item.Meta>
                            <Item.Description>
                                <label>수량</label>
                                <div className="button_group">
                                    <Input>
                                        <Button basic icon className="minus_count">
                                            <Icon className="minus"/>
                                        </Button>
                                        <input defaultValue={1}/>
                                        <Button basic icon className="plus_count">
                                            <Icon className="plus"/>
                                        </Button>
                                    </Input>
                                </div>
                            </Item.Description>

                            <Item.Extra>
                                <Divider/>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width={8}>
                                            <label>총 금액</label>
                                        </Grid.Column>
                                        <Grid.Column width={8} textAlign="right">
                                            <div className="total_price">
                                                150,000 <span>원</span>
                                            </div>
                                        </Grid.Column>
                                    </Grid.Row>
                                        
                                    <Grid.Row className="button_group">
                                        <Grid.Column width={2}>
                                            <Button basic icon fluid size="large" >
                                                <Icon  className="heart outline"/>
                                            </Button>
                                        </Grid.Column>
                                        <Grid.Column width={7}>
                                            <Button  icon fluid basic size="large" className="basket">
                                                장바구니
                                            </Button>
                                        </Grid.Column>
                                        <Grid.Column width={7}>
                                            <Button icon fluid size="large" primary>
                                                바로구매
                                            </Button>
                                        </Grid.Column>
                                    </Grid.Row>
                         
                                </Grid>
                            </Item.Extra>
                        </Item.Content>
                        </Item>
                    </Grid.Column>
                </Grid>
            </Container>
            <Container>
                <DetailTab></DetailTab>
            </Container>
            </>
        );
    }
}

export default DetailProduct;