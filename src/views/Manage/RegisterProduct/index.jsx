import React ,{Component} from 'react';
import { Header, Container, Segment ,Form, Button, Grid, Modal, Item, Image, Input, Table} from 'semantic-ui-react';


const options = [
    { key: '1', text: '가전', value: '가전' },
    { key: '2', text: '패션/잡화', value: '패션/잡화' },
    { key: '3', text: '뷰티', value: '뷰티' },
  ]
  

function exampleReducer(state, action) {
    switch (action.type) {
      case 'close':
        return { open: false }
      case 'open':
        return { open: true}
      default:
        throw new Error('Unsupported action...')
    }
  }

const RegisterProduct =() =>  {

        const [state, dispatch] = React.useReducer(exampleReducer, {
            open: false,
        })
        const { open } = state

        return(
            <>
            <Container>
                <div className="title_area">
                    <Header as="h2">상품등록</Header>
                </div>
                <Segment className="form_wrap">
                    <Grid>
                        <Grid.Row>
                            <Grid.Column className="form_title">상품정보</Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Form>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={2} textAlign="right" verticalAlign="middle">
                                <label>상품명</label>
                            </Grid.Column>
                            <Grid.Column width={13}>
                                <input type="text" placeholder="상품명을 입력하세요"/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={3}>
                            <Grid.Column width={2} textAlign="right" verticalAlign="middle">
                                <label>브랜드</label>
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <input type="text" placeholder="삼성전자"/>
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Form.Button inline basic content='선택하기'  onClick={() => dispatch({ type: 'open'})} />
                            </Grid.Column>

                            <Grid.Column width={2} verticalAlign="middle"> 
                                <label>상품분류</label>
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Form.Select 
                                    options={options}
                                    placeholder='상품분류를 선택하세요'
                                    inline
                                />
                            </Grid.Column>                 
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column width={2} textAlign="right" verticalAlign="middle">
                                <label>상품가격</label>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <input type="text" placeholder="판매가격을 입력하세요"/>
                            </Grid.Column>
                            <Grid.Column width={2}  verticalAlign="middle">
                                <label>수량</label>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <input type="text" placeholder="판매수량을 입력하세요"/>
                            </Grid.Column>
                        </Grid.Row>
                        </Grid>  
                    </Form>
                </Segment>
                <Segment >
                    <Grid>
                        <Grid.Row>
                            <Grid.Column className="form_title">이미지 등록</Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Form>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={3} textAlign="right" verticalAlign="middle">
                                    대표 이미지 등록
                                </Grid.Column>
                                <Grid.Column width={9}>
                                    <Form.Input placeholder="productdetail_o1.jpg(1.90MB)" />
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <Form.Button content="이미지 삭제" basic/>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column width={3} textAlign="right" verticalAlign="middle">
                                    상세 이미지 등록
                                </Grid.Column>
                                <Grid.Column width={9}>
                                    <Form.Input placeholder="이미지를 선택하세요" type="file"/>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <Form.Button content="이미지 선택" basic/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Form>
                </Segment>
                <Button.Group>
                    <Button basic size="large">등록취소</Button>
                    <Button primary size="large">상품 등록하기</Button>
                </Button.Group>
            </Container>


            
            <Modal
            open={open}
            onClose={() => dispatch({ type: 'close' })}
            size="tiny"
            centered={false}
        >
            <Modal.Header>브랜드 선택</Modal.Header>
            <Modal.Content>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                            <label>브랜드 검색</label>
                        </Grid.Column>
                        <Grid.Column width={8}> 
                            <Input placeholder="검색어를 입력하세요"/>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Button content="검색"/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Table selectable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>브랜드</Table.HeaderCell>
                            <Table.HeaderCell>제조국</Table.HeaderCell>
                            <Table.HeaderCell>설립년도</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row textAlign="left">
                            <Table.Cell>
                                <Image src="images/temp/lg_logo.png" size="tiny" inline bordered te/>
                                <span>LG전자</span>
                            </Table.Cell>
                            <Table.Cell>대한민국</Table.Cell>
                            <Table.Cell>1947년</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Image src="images/temp/lg_logo.png" size="tiny" inline bordered/>
                                <span>삼성전자</span>
                            </Table.Cell>
                            <Table.Cell>대한민국</Table.Cell>
                            <Table.Cell>1947년</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Image src="images/temp/lg_logo.png" size="tiny" inline bordered/>
                                <span>BOSE(보스)</span>
                            </Table.Cell>
                            <Table.Cell>미국</Table.Cell>
                            <Table.Cell>1947년</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Modal.Content>
            <Modal.Actions>
            <Button  centered  onClick={() => dispatch({ type: 'close' }) } >
                취소
            </Button>
            <Button  centered primary onClick={() => dispatch({ type: 'close' }) } >
                확인
            </Button>
            </Modal.Actions>
        </Modal>
            </>
        )
    }


export default RegisterProduct;