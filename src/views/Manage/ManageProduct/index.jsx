import React , {Component} from 'react';
import { Container, Grid, Table,Item, Icon ,Image, Button, Header, Dropdown, Search, Segment, Input} from 'semantic-ui-react';
import {Link} from 'react-router-dom';

export default class ManageProduct extends Component {
    render() {
        const options = [
            { key: '가전', text: '가전', value: '가전'},
            { key: '패션/잡화', text: '패션/잡화', value: '패션/잡화' },
            { key: '뷰티', text: '뷰티', value: '뷰티' },
          ]
        return(
            <>
            <Container>
                <Grid>
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <Header className="title_header">상품관리</Header>
                        </Grid.Column>
                        <Grid.Column textAlign="right">
                            <Link to="/register">
                                <Button basic size="large">상품등록</Button>                           
                            </Link>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Segment className="search_wrap">
                    <Grid verticalAlign="middle">
                        <Grid.Row columns={5}>
                            <Grid.Column width={2} textAlign="right">
                                상품분류
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Dropdown
                                selection fluid options={options} defaultValue={options[0].value}/>
                            </Grid.Column>
                            <Grid.Column width={1} textAlign="right">
                                상품명
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <Input fluid placeholder='상품명을 입력하세요' />
                            </Grid.Column>
                            <Grid.Column width={4} textAlign="right">
                                <Button.Group>
                                    <Button size="large" basic>초기화</Button>
                                    <Button size="large" primary>검색하기</Button>
                                </Button.Group>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Segment>
                <p>상품<span className="bold">4</span>개</p>
 
                <Table textAlign="center" basic="very" padded>
                    <Table.Header>
                        <Table.HeaderCell className="first_child">상품이미지</Table.HeaderCell>
                        <Table.HeaderCell>상품번호</Table.HeaderCell>
                        <Table.HeaderCell>상품분류</Table.HeaderCell>
                        <Table.HeaderCell>상품명</Table.HeaderCell>
                        <Table.HeaderCell>제조사</Table.HeaderCell>
                        <Table.HeaderCell>가격</Table.HeaderCell>
                        <Table.HeaderCell>등록일</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Header>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>
                                <Image src="images/temp/product-01.png" size="mini" centered verticalAlign="middle"/>
                            </Table.Cell>
                            <Table.Cell>12345670</Table.Cell>
                            <Table.Cell>가전</Table.Cell>
                            <Table.Cell>[BOSS] 보스 사운드링크 리볼브+ 블루투스 스피커</Table.Cell>
                            <Table.Cell>Boss</Table.Cell>
                            <Table.Cell>150,000</Table.Cell>
                            <Table.Cell>2020.00.00 00:00</Table.Cell>
                            <Table.Cell className="edit">
                                <Button basic>
                                    <Icon className="trash alternate outline"/>
                                </Button>
                                <Button basic>
                                    <Icon className="edit outline"/>
                                </Button>
                            </Table.Cell>
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>
                                <Image src="images/temp/product-05.png" size="mini" centered verticalAlign="middle"/>
                            </Table.Cell>
                            <Table.Cell>12345670</Table.Cell>
                            <Table.Cell>패션/잡화</Table.Cell>
                            <Table.Cell>JACQUARD COTTON MINI DRESS(JS20D001PU)</Table.Cell>
                            <Table.Cell>대한민국</Table.Cell>
                            <Table.Cell>79,000</Table.Cell>
                            <Table.Cell>2020.00.00 00:00</Table.Cell>
                            <Table.Cell className="edit">
                                <Button basic>
                                    <Icon className="trash alternate outline"/>
                                </Button>
                                <Button basic>
                                    <Icon className="edit outline"/>
                                </Button>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Container>
            </>
        )
    }
}