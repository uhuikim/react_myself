import React, { Component } from 'react';
import { Menu, Icon } from 'semantic-ui-react';

export default class MenuLayout extends Component {
state = {
  activeItem:"홈",
}
handleItemClick= (e, { name }) => this.setState({ activeItem: name })


  render() {
        const { activeItem } = this.state;
        const { children } = this.props;
    
        return (
          <>
            <Menu pointing secondary>
              <Menu.Item header>
                  <Icon name="sidebar"/>
                  카테고리
              </Menu.Item>
              <Menu.Item
              name="홈"
              active={activeItem === '홈'}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name="가전"
              active={activeItem === '가전'}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name="패션/잡화"
              active={activeItem === '패션/잡화'}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name="뷰티"
              active={activeItem === '뷰티'}
              onClick={this.handleItemClick}
            />
            </Menu>
            <main>{children}</main>
          </>
        );
  }
}