import React, {Component} from 'react';
import {Container,Search,Image, Header, Menu, Dropdown,Icon} from 'semantic-ui-react';
import MenuLayout from './CustomerMenu'
import { Link} from 'react-router-dom';

class HeaderLayout extends Component {

    render() {
        const {children} = this.props;
        const loginOptions =
        [{
            key: 'Customer',
            text: 'Customer',
            value: 'Customer',
          },
          {
            key: 'Manager',
            text: 'Manager',
            value: 'Manager',
          },]

        return(
            <>
            <div className="header-section">
                <div className="fixed-header">
                    <Container>
                        <Menu secondary>
                            <Link to="/">
                                <Menu.Item>
                                    <Header>
                                        Namoo<span>Mall</span>
                                    </Header>
                                </Menu.Item>
                            </Link>
                            <Menu.Item>
                                <Search/>
                            </Menu.Item>
                            <Menu.Menu position="right">
                                    <Menu.Item>
                                        <div className="user-info">
                                            <div className="top-area">
                                                <span className="bold">홍길동</span>님
                                                <Dropdown
                                                inline
                                                options={loginOptions}
                                                defaultValue={loginOptions[0].value}/>
                                            </div>
                                            <div className="bottom-area">
                                                <p>CJ ONE 포인트<span>12,000</span>P</p>
                                            </div>
                                        </div>
                                        <Image src="/images/icons/icon-profile.svg" alt=""/>
                                    </Menu.Item>
                                <Menu.Item>
                                    <Link to="/basket">
                                    <div className="cart">
                                        <Image src="/images/icons/icon-cart.svg" alt=""/>
                                        <span>장바구니</span>
                                    </div>
                                    </Link>
                                </Menu.Item>
                            </Menu.Menu>
                        </Menu>
                    </Container>
                </div>
                <div className="gnb">
                    <Container>
                        <MenuLayout/>
                    </Container>
                </div>
            </div>
            <main>
                {children}
            </main>
        </>
        )
    }
}

export default HeaderLayout;