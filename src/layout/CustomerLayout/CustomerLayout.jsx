import React ,{Component} from 'react';
import {Route} from 'react-router-dom';

import CustomerHeader from './CustomerHeader'

 class CustomerLayout extends Component{
    render() {
    const {component :  Component, ...rest} = this.props;
        return(
            <Route {...rest} render={(routerprops)=>
                <CustomerHeader>
                    <Component {...routerprops}/>
                </CustomerHeader>
            }
            />
        )
    }
}

export default CustomerLayout;