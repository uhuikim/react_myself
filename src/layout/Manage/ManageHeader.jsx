import React, {Component} from 'react';
import {Container,Search,Image, Header, Menu, Dropdown,Icon} from 'semantic-ui-react';
import ManagerMenu from './ManageMenu'
import { Link} from 'react-router-dom';

class ManagerHeader extends Component {

    render() {
        const {children} = this.props;
        const loginOptions =
        [{
            key: 'Customer',
            text: 'Customer',
            value: 'Customer',
          },
          {
            key: 'Manager',
            text: 'Manager',
            value: 'Manager',
          },]

        return(
            <>
            <div className="header-section">
                <div className="fixed-header">
                    <Container>
                        <Menu secondary>
                            <Link to="/">
                                <Menu.Item>
                                    <Header>
                                        Namoo<span>Mall</span>
                                    </Header>
                                </Menu.Item>
                            </Link>
                            <Menu.Item>
                                <Search/>
                            </Menu.Item>
                            <Menu.Menu position="right" className="manager_info">
                                <Menu.Item>
                                    <div className="user-info">
                                        <div className="top-area">
                                            <span className="bold">홍길동</span>님
                                            안녕하세요
                                            
                                        </div>
                                        <div className="bottom_area">
                                            <Dropdown
                                                inline
                                                options={loginOptions}
                                                defaultValue={loginOptions[1].value}/>
                                        </div>
                                    </div>
                                    <Image src="/images/icons/icon-profile.svg" alt=""/>
                                </Menu.Item>
                            </Menu.Menu>
                        </Menu>
                    </Container>
                </div>
                <div className="gnb">
                    <Container className="manager_menu_wrap">
                        <ManagerMenu/>
                    </Container>
                </div>
            </div>
            <main>
                {children}
            </main>
        </>
        )
    }
}

export default ManagerHeader