import React,{Component} from 'react';
import {Menu,Image} from 'semantic-ui-react';
import {Link} from 'react-router-dom';

export default class ManagerMenu extends Component {

    state = {activeItem : "managehome"}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })


    render() {
        const { activeItem } = this.state

        return (
            <>
          <Menu secondary pointing>
            <Menu.Item
              name='manageproduct'
              active={activeItem === 'manageproduct'}
              onClick={this.handleItemClick}
              className = "icon-shop-wrap"
              
            >
              <Image src="/images/icons/icon-shop.svg"/>
              관리자홈
            </Menu.Item>

            <Link to="/brand_register">
              <Menu.Item
                name='brand'
                active={activeItem === 'brand'}
                onClick={this.handleItemClick}
              >
                브랜드등록
              </Menu.Item> 
            </Link>
            <Link to="/register">
            <Menu.Item
              name='managehome'
              active={activeItem === 'managehome'}
              onClick={this.handleItemClick}
            >
              상품관리
            </Menu.Item> 
            </Link>  
          </Menu>
          </>
        );
    }
}