import React ,{Component} from 'react';
import {Route} from 'react-router-dom';

import  ManageHeader from './ManageHeader'

 class ManageLayout extends Component{
    render() {
    const {component :  Component, ...rest} = this.props;
        return(
            <Route {...rest} render={(routerprops)=>
                <ManageHeader>
                    <Component {...routerprops}/>
                </ManageHeader>
            }
            />
        )
    }
}

export default ManageLayout;