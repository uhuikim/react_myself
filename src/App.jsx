import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

//Layout 
import CustomerLayout from './layout/CustomerLayout/CustomerLayout' 
import ManageLayout from './layout/Manage/ManageLayout'


//content
import ProductList from './views/Customer/ProductList/index'
import Basket from './views/Customer/Basket'
import ProductDetail from './views/Customer/DetailProduct'
import Purchase from './views/Customer/Purchase/index'
import PurchaseFinish from './views/Customer/PurchaseFinish'

import MangeProduct from './views/Manage/ManageProduct/index'
import RegisterProduct from './views/Manage/RegisterProduct/index'


class App extends Component {
  render() {
    return (
    <Router>
      <Switch>
        <CustomerLayout exact path="/" component= {ProductList}/>
        <CustomerLayout  path="/basket" component= {Basket}/>
        <CustomerLayout  path="/detail" component= {ProductDetail}/>
        <CustomerLayout  path="/purchase" component= {Purchase}/>
        <CustomerLayout  path="/finish_purchase" component= {PurchaseFinish}/>


      <ManageLayout path='/manage' component={MangeProduct}/>
      <ManageLayout path='/register' component={RegisterProduct}/>
      </Switch>
    </Router>    

    );
  }
}

export default App;
